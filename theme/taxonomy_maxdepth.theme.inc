<?php

/**
 * @file
 * Theming function.
 */

/**
 * Theming function that rewrites the basic one theme_taxonomy_overview_terms.
 */
function theme_taxonomy_maxdepth_taxonomy_overview_terms(array $variables) {

  $form = $variables['form'];

  $vocabulary = taxonomy_vocabulary_load($form['#vocabulary']->vid);
  $max_depth = isset($vocabulary->maxdepth) ? $vocabulary->maxdepth : 0;

  $page_entries = $form['#page_entries'];
  $back_step = $form['#back_step'];
  $forward_step = $form['#forward_step'];

  // Add drag and drop if parent fields are present in the form.
  if ($form['#parent_fields'] && $max_depth != 1) {

    $depth = $max_depth;
    if ($max_depth > 1) {
      $depth = $max_depth - 1;
    }

    drupal_add_tabledrag('taxonomy', 'match', 'parent', 'term-parent', 'term-parent', 'term-id', FALSE);
    drupal_add_tabledrag('taxonomy', 'depth', 'group', 'term-depth', NULL, NULL, FALSE);
    drupal_add_js(drupal_get_path('module', 'taxonomy_maxdepth') . '/js/taxonomy_maxdepth.js');
    drupal_add_js(
      array(
        'taxonomy' => array(
          'backStep' => $back_step,
          'forwardStep' => $forward_step,
          'maxDepth' => $depth,
        ),
      ),
      'setting');
    drupal_add_css(drupal_get_path('module', 'taxonomy') . '/taxonomy.css');
  }

  drupal_add_tabledrag('taxonomy', 'order', 'sibling', 'term-weight');

  $errors = form_get_errors() != FALSE ? form_get_errors() : array();
  $rows = array();

  $current_depth = 0;
  $last_tid = 0;
  $current_parent = 0;

  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#term'])) {
      $term = &$form[$key];

      // Move up one level in depth
      if ($term['#term']['parent'] != 0 && $current_parent != $term['#term']['parent']) {
        $current_depth += 1;
        $current_parent = $term['#term']['parent'];
      }

      // Reset on first level
      if ($term['#term']['parent'] == 0) {
        $current_depth = 0;
      }

      // If max depth < current term depth && different than 0 (unlimited).
      if ($current_depth > $max_depth && $max_depth != 0) {
        // Reset parent.
        $term['#term']['parent'] = $last_tid;
        $term['parent']['#default_value'] = $last_tid;
        $term['parent']['#value'] = $last_tid;

        $term['#term']['depth'] = $max_depth;
        $current_depth -= 1;
      }

      // If no depth allowed.
      if ($max_depth == 1) {
        // Reset all.
        $term['#term']['parent'] = 0;
        $term['parent']['#default_value'] = 0;
        $term['parent']['#value'] = 0;
        $term['#term']['depth'] = 0;
      }

      $row = array();
      $row[] = (isset($term['#term']['depth']) && $term['#term']['depth'] > 0 ? theme('indentation', array('size' => $term['#term']['depth'])) : '') . drupal_render($term['view']);
      if ($form['#parent_fields']) {
        $term['tid']['#attributes']['class'] = array('term-id');
        $term['parent']['#attributes']['class'] = array('term-parent');
        $term['depth']['#attributes']['class'] = array('term-depth');
        $row[0] .= drupal_render($term['parent']) . drupal_render($term['tid']) . drupal_render($term['depth']);
      }
      $term['weight']['#attributes']['class'] = array('term-weight');
      $row[] = drupal_render($term['weight']);
      $row[] = drupal_render($term['edit']);
      $row = array('data' => $row);
      $rows[$key] = $row;

      $last_tid = $term['#term']['tid'];

    }
  }

  // Add necessary classes to rows.
  $row_position = 0;
  foreach ($rows as $key => $row) {
    $rows[$key]['class'] = array();
    if (isset($form['#parent_fields'])) {
      $rows[$key]['class'][] = 'draggable';
    }

    // Add classes that mark which terms belong to previous and next pages.
    if ($row_position < $back_step || $row_position >= $page_entries - $forward_step) {
      $rows[$key]['class'][] = 'taxonomy-term-preview';
    }

    if ($row_position !== 0 && $row_position !== count($rows) - 1) {
      if ($row_position == $back_step - 1 || $row_position == $page_entries - $forward_step - 1) {
        $rows[$key]['class'][] = 'taxonomy-term-divider-top';
      }
      elseif ($row_position == $back_step || $row_position == $page_entries - $forward_step) {
        $rows[$key]['class'][] = 'taxonomy-term-divider-bottom';
      }
    }

    // Add an error class if this row contains a form error.
    foreach ($errors as $error_key => $error) {
      if (strpos($error_key, $key) === 0) {
        $rows[$key]['class'][] = 'error';
      }
    }
    $row_position++;
  }

  if (empty($rows)) {
    $rows[] = array(
      array(
        'data' => $form['#empty_text'],
        'colspan' => '3',
      ),
    );
  }

  $header = array(t('Name'), t('Weight'), t('Operations'));
  $output = theme('table',
    array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => 'taxonomy',
      ),
    ));
  $output .= drupal_render_children($form);
  $output .= theme('pager');

  return $output;
}
